FROM debian:buster-slim


RUN export DEBIAN_FRONTEND=noninteractive
RUN apt update -y && apt upgrade -y && apt install ncat curl iptables -y

COPY initAPI.sh initAPI.sh
COPY healthcheck.sh healthcheck.sh
