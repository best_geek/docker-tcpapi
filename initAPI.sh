#!/bin/bash

#clean any iptables rules left behind
currentloaded=$(iptables -S | grep "$LISTEN_PORT -j" | sed -e 's/-A //g')


#so more than 1 we should remove some rules
#can be seen in /tmp folder on contanier
if  [ $(echo "$currentloaded" | wc -l) -gt 1 ]
	 then 
        	echo "there are existing rules should flush" > /tmp/ruleflush.txt
        	echo "$currentloaded" | while read line; do
		echo "removed $line" >> /tmp/ruleflush.txt
                iptables -D $line
        done 


        else
       		 echo "no rules to remove" > /tmp/ruleflush.txt

fi


#see if vars are set
if (echo $LISTEN_FROM | grep -q "ANY") 
	then
		echo "detect accept from ANY device in enviroment. I will not be setting iptables" > /tmp/detect_iptables

	else
		echo "detect strict source filtering. Should apply iptables on $LISTEN_FROM for port $LISTEN_PORT" > /tmp/detect_iptables
		#set iptables rules
		iptables -A INPUT -p tcp --dport $LISTEN_PORT -s $LISTEN_FROM -j ACCEPT
		iptables -A INPUT -p tcp --dport $LISTEN_PORT -j DROP
fi




#start ncat listener
ncat -lknvp $LISTEN_PORT -c "ncat -U /var/run/docker.sock"

#easy hack to leave running
/usr/bin/tail -f /dev/null
